﻿using TestDoublesStory.Interfaces;
using TestDoublesStory.Models;

namespace TestDoublesStory
{
    public class DefaultReservationService : IReservationService
    {
        private readonly IHotelRepository _hotelRepository;
        private readonly IReservationRepository _reservationRepository;

        public DefaultReservationService(IHotelRepository hotelRepository, IReservationRepository reservationRepository)
        {
            _hotelRepository = hotelRepository;
            _reservationRepository = reservationRepository;
        }

        public void ReserveBestHotelInCity(string city, DateTime startDate, DateTime endDate)
        {
            var allHotelsInCity = _hotelRepository.FindHotelsByCity(city);
            var sortedHotels = SortByRank(allHotelsInCity);
            foreach (var hotel in sortedHotels)
            {
                var hotelOpenForReservation = _reservationRepository.IsOpenForReservation(hotel.Id, startDate, endDate);
                if (hotelOpenForReservation)
                {
                    _reservationRepository.Reserve(hotel.Id, startDate, endDate);
                    return;
                }
            }
        }

        private List<Hotel> SortByRank(List<Hotel> allHotelsInCity)
        {
            return allHotelsInCity.OrderBy(h => h.Rank).ToList();
        }
    }
}
