﻿using TestDoublesStory.Models;

namespace TestDoublesStory.Interfaces
{
    public interface IHotelRepository
    {
        List<Hotel> FindHotelsByCity(string city);
    }
}
