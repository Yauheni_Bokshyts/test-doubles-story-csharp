﻿
namespace TestDoublesStory.Interfaces
{
    public interface IReservationService
    {
        void ReserveBestHotelInCity(string city, DateTime startDate, DateTime endDate);
    }
}
